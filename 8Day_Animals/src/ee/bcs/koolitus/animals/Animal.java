package ee.bcs.koolitus.animals;

public abstract class Animal {
	private String spiecesName;
	private Gender gender;

	public abstract void moveAhead();

	public String getSpiecesName() {
		return spiecesName;
	}

	public void setSpiecesName(String spiecesName) {
		this.spiecesName = spiecesName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;

	}

}
