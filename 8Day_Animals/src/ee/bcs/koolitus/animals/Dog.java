package ee.bcs.koolitus.animals;

public class Dog extends Animal{
	private String sleepingPlace;
	
	@Override
	public void moveAhead() {
		System.out.println("Running lazily");
	}

	public String getSleepingPlace() {
		return sleepingPlace;
	}

	public void setSleepingPlace(String sleepingPlace) {
		this.sleepingPlace = sleepingPlace;
	}
}
