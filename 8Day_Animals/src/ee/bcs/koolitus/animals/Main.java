package ee.bcs.koolitus.animals;


import ee.bcs.koolitus.animals.Animal;
import ee.bcs.koolitus.animals.Dog;
import ee.bcs.koolitus.animals.Gender;

public class Main {

	public static void main(String[] args) {
		// Animal a = new Animal(); ei saa teha kuna klass on abstraktne
		Dog kiki = new Dog();
		kiki.setGender(Gender.FEMALE);
		kiki.setSleepingPlace("travelbag");
		kiki.setSpiecesName(
				kiki.getClass().getSimpleName());
		kiki.moveAhead();
		System.out.println("Kiki is a " + kiki.getSpiecesName());
	}

}

