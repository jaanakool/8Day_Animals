package ee.bcs.koolitus.animals;


public class Dolphin extends Animal {
	private Side whichBrainSideActive;

	@Override
	public void moveAhead() {
		System.out.println("swims ahead");
	}

	public Side getWhichBrainSideActive() {
		return whichBrainSideActive;
	}

	public void setWhichBrainSideActive(
			Side whichBrainSideActive) {
		this.whichBrainSideActive = whichBrainSideActive;
	}
}
